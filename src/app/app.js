import angular from 'angular';

import carouselData from './service/carouselData.service';
import carousel from './components/Carousel/carousel.directive';
import Maincontroller from './main/main.controller';

angular.module('app', [])
	.service('carouselData', carouselData)
	.controller('Maincontroller', Maincontroller)
	.directive('carousel', carousel);