const cards = [{
					"name": "card-1",
					"image" : "image/logo.png",
					"text" : "et, consectetur adipisicing elit. Explicabo, laborum!",
					"color" : {
						'background-color':'#5F9EA0'
					}
				},
				 {
					"name": "card-2",
					"image" : "image/logo.png",
					"text" : "11111111111 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, laborum!",
					"color" : {
						'background-color':'#6A5ACD'
					}
				},
				{
					"name": "card-3",
					"image" : "image/logo.png",
					"text" : "6666666 rsit amet, consectetur adipisicing elit. Explicabo, laborum!",
					"color" : {
						'background-color':'#00FFFF'
					}
				},
				{
					"name": "card-4",
					"image" : "image/logo.png",
					"text" : "dfghdfhsdfh sdhsdhsdh amet, consectetur adipisicing elit. Explicabo, laborum!",
					"color" : {
						'background-color':'#363636'
					}
				},
				{
					"name": "card-5",
					"image" : "image/logo.png",
					"text" : "5555555555 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, laborum!",
					"color" : {
						'background-color':'#90EE90'
					}

				},
				{
					"name": "card-6",
					"image" : "image/logo.png",
					"text" : "sfasgesahgsdhsds Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, laborum!",
					"color" : {
						'background-color':'#FFFACD'
					}
				},
			  {
					"name": "card-7",
					"image" : "image/logo.png",
					"text" : "54645643457345 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, laborum!",
					"color" : {
						'background-color':'#FA8072'
					}
				}];

export default cards;