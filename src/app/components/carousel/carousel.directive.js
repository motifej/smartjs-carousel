import './carousel.css';

export default function carouselDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/carousel/carousel.html',
    link: linkFunc,
    scope: {
        cards: '=',
        action: '&'
    }
  };

  return directive;

  function linkFunc(scope, el) {
    el.on('mouseover', e => {
      if(e.target.className.indexOf('card') === -1) {
        return;
      }
      el.addClass('stopAnimate');
    });

    el.on('mouseout', () => el.removeClass('stopAnimate'));
	}
}