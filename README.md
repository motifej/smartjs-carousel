## Installation

To use it, just clone this repo and install the npm dependencies:

```shell
$ git clone https://motifej@bitbucket.org/motifej/smartjs-carousel.git my_app
$ cd my_app
$ npm install
```

## Scripts

All scripts are run with `npm run [script]`, for example: `npm run dev`.

* `build` - generate a minified build to dist folder
* `dev` - start development server, try it by opening `http://localhost:8080/`
